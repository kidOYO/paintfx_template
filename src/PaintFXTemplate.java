import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class PaintFXTemplate extends Application {

	public Canvas canvas;			
	public BorderPane root;			
	public Scene scene;
	public GraphicsContext gc;		

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		initWindows();
		initDrawing();
		initEventHandlers();
		setStage(primaryStage);
	}
	
	
	public void initWindows() {
		
	}
	
	public void initDrawing(){
		
	}
	
	public void initEventHandlers() {

		canvas.addEventHandler(MouseEvent.MOUSE_PRESSED, 
				new EventHandler<MouseEvent>(){

			@Override
			public void handle(MouseEvent event) {
				
			}
		});


		canvas.addEventHandler(MouseEvent.MOUSE_DRAGGED, 
				new EventHandler<MouseEvent>(){
			@Override
			public void handle(MouseEvent event) {
				
			}
		});


		canvas.addEventHandler(MouseEvent.MOUSE_RELEASED, 
				new EventHandler<MouseEvent>(){
			@Override
			public void handle(MouseEvent event) {	
				
			}
		});
	}
	
	public void setStage(Stage primaryStage) {
		
	}
	
}